from typing import Optional, List

from fastapi import APIRouter, HTTPException
from pydantic import BaseModel
from sqlalchemy import desc
from sqlalchemy.exc import IntegrityError

from ticketing.database import get_session
from ticketing.database.model import Product

router = APIRouter(
    prefix="/product",
    tags=["product"],
)


class ProductCreate(BaseModel):
    name: str
    version: str


@router.post("", status_code=201)
def create_product(product_data: ProductCreate):
    session = get_session()
    try:
        product = Product(name=product_data.name, version=product_data.version)
        session.add(product)
        session.commit()
    except IntegrityError:
        session.rollback()
        return HTTPException(
            status_code=409, detail="Product with same version already exists"
        )

    return {
        "message": "Product created successfully",
        "data": {
            "id": product.id,
            "name": product_data.name,
            "version": product_data.version,
        },
    }


@router.get("")
def get_product(name: Optional[str] = "", version: Optional[str] = ""):
    session = get_session()
    if name and version:
        product = (
            session.query(Product)
            .filter_by(name=name, version=version, deleted_at=None)
            .first()
        )
        if product is None:
            return HTTPException(status_code=404, detail="Product not found")

        return {
            "message": "OK",
            "data": {
                "id": product.id,
                "name": product.name,
                "version": product.version,
            },
        }

    products = (
        session.query(Product)
        .filter_by(deleted_at=None)
        .order_by(desc(Product.created_at))
        .all()
    )
    return {
        "message": "OK",
        "data": [
            {
                "id": product.id,
                "name": product.name,
                "version": product.version,
                "created_at": product.created_at,
            }
            for product in products
        ],
    }


@router.delete("")
def delete_product(name: str, version: Optional[str] = ""):
    session = get_session()

    product: Optional[List[Product]]
    if version:
        product = session.query(Product).filter_by(name=name, version=version).all()
    else:
        product = session.query(Product).filter_by(name=name).all()

    if product is None:
        return HTTPException(status_code=404, detail="Product not found")

    for p in product:
        p.delete()

    session.commit()
    return {"message": "Product deleted successfully"}


@router.post("/{id}")
def update_product(id: str, product_data: ProductCreate):
    session = get_session()
    product = session.query(Product).filter_by(id=id).first()
    if not product:
        return HTTPException(status_code=404, detail="Product not found")

    product.name = product_data.name
    product.version = product_data.version
    session.commit()

    return {
        "message": "Product updated successfully",
        "data": {
            "id": product.id,
            "name": product.name,
            "version": product.version,
        },
    }

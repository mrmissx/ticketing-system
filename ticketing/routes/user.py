from datetime import datetime
from typing import Optional
from enum import Enum

from fastapi import APIRouter, HTTPException, Body, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from sqlalchemy.exc import IntegrityError

from ticketing.database import get_session
from ticketing.database.model import User
from ticketing.utils.auth import generate_token, verify_token

router = APIRouter(
    prefix="/user",
    tags=["user"],
)


class Role(str, Enum):
    user = "user"
    cs = "cs"


class UserBase(BaseModel):
    username: str
    password: str
    role: Role


class UserUpdate(BaseModel):
    username: Optional[str]
    password: Optional[str]
    role: Optional[Role]


@router.get("/")
def index(role: Optional[str] = None):
    session = get_session()
    if role:
        user = (
            session.query(User).filter(User.deleted_at == None, User.role == role).all()
        )
    else:
        user = session.query(User).filter(User.deleted_at == None).all()
    return user


@router.post("/", status_code=201)
def create_user(user_data: UserBase):
    session = get_session()
    try:
        user = User(user_data.username, user_data.password, user_data.role)
        session.add(user)
        session.commit()
    except IntegrityError:
        raise HTTPException(status_code=400, detail="Username already exists")
    return {
        "message": "User created successfully",
        "data": {
            "id": user.id,
            "username": user.username,
            "role": user.role,
            "created_at": user.created_at,
        },
    }


@router.patch("/")
def update_user(user_data: UserUpdate, current_user: User = Depends(verify_token)):
    session = get_session()
    user: Optional[User] = (
        session.query(User).filter_by(username=current_user.get("username")).first()
    )
    if not user:
        raise HTTPException(status_code=400, detail="User not found")

    for key, value in user_data.dict(exclude_unset=True).items():
        setattr(user, key, value)

    return {
        "OK": True,
        "data": {
            "id": user.id,
            "username": user.username,
            "role": user.role,
        },
    }


@router.delete("/")
def delete_user(username: str = Body(embed=True)):
    session = get_session()
    user = session.query(User).filter(User.username == username)
    if not user:
        raise HTTPException(status_code=400, detail="User not found")
    user.update({"deleted_at": datetime.utcnow()})
    session.commit()
    return {"message": "User deleted successfully"}


@router.post("/login")
def login(username: str = Body(), password: str = Body()):
    session = get_session()
    user = (
        session.query(User)
        .filter(
            User.username == username,
            User.password == password,
            User.deleted_at == None,
        )
        .first()
    )
    if not user:
        raise HTTPException(status_code=400, detail="Invalid credentials")

    token = generate_token(
        {"id": user.id, "username": user.username, "role": user.role}
    )

    return JSONResponse(
        {"status": "OK", "access_token": token},
        status_code=200,
        headers={"Authorization": f"Bearer {token}"},
    )


@router.get("/me")
def me(user: User = Depends(verify_token)):
    return user

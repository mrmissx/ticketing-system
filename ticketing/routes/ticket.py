from fastapi import APIRouter, Depends, HTTPException, Body
from pydantic import BaseModel
from sqlalchemy.sql import func

from ticketing.database import get_session
from ticketing.database.model import User, Ticket
from ticketing.utils.auth import verify_token

router = APIRouter(
    prefix="/ticket",
    tags=["ticket"],
)


class CreateTicket(BaseModel):
    title: str
    problem: str
    product_name: str
    product_version: str


@router.post("/")
def create_ticket(ticket_data: CreateTicket, user: User = Depends(verify_token)):
    if user["role"] != "user":
        raise HTTPException(status_code=403, detail="Only user can create ticket")

    session = get_session()
    cs = (
        session.query(User)
        .filter_by(role="cs", deleted_at=None)
        .order_by(func.random())
        .first()
    )
    if not cs:
        raise HTTPException(status_code=400, detail="No CS available")
    ticket = Ticket(
        title=ticket_data.title,
        problem=ticket_data.problem,
        product_name=ticket_data.product_name,
        product_version=ticket_data.product_version,
        creator_id=user["id"],
        cs_id=cs.id,
    )
    session.add(ticket)
    session.commit()

    return {
        "message": "Ticket created successfully",
        "data": {
            "id": ticket.id,
            "title": ticket.title,
            "problem": ticket.problem,
            "product_name": ticket.product_name,
            "product_version": ticket.product_version,
            "status": ticket.status,
            "cs": {"id": cs.id, "username": cs.username},
        },
    }


@router.post("/close")
def close_ticket(ticket_id: str, user: User = Depends(verify_token)):
    if user["role"] != "cs":
        raise HTTPException(status_code=403, detail="Only CS can close ticket")

    session = get_session()
    ticket = session.query(Ticket).filter_by(id=ticket_id).first()
    if not ticket:
        raise HTTPException(status_code=404, detail="Ticket not found")
    if ticket.status == "close":
        raise HTTPException(status_code=400, detail="Ticket already closed")
    if ticket.cs_id != user["id"]:
        raise HTTPException(status_code=403, detail="You are not the CS of this ticket")
    ticket.status = "close"
    session.commit()

    return {"message": "Ticket closed successfully"}


@router.post("/pass")
def pass_ticket(
    ticket_id: str, cs: str = Body(embed=True), user: User = Depends(verify_token)
):
    if user["role"] != "cs":
        raise HTTPException(status_code=403, detail="Only CS can pass ticket")

    session = get_session()
    ticket = session.query(Ticket).filter_by(id=ticket_id).first()
    new_cs = session.query(User).filter_by(id=cs).first()
    if not new_cs or new_cs.role != "cs":
        raise HTTPException(status_code=404, detail="CS not found")
    if new_cs.id == user["id"]:
        raise HTTPException(
            status_code=400, detail="You are already the CS of this ticket"
        )
    if not ticket:
        raise HTTPException(status_code=404, detail="Ticket not found")

    if ticket.cs_id != user["id"]:
        raise HTTPException(status_code=403, detail="You are not the CS of this ticket")
    if ticket.status == "close":
        raise HTTPException(status_code=400, detail="Ticket already closed")
    ticket.cs_id = new_cs.id
    session.commit()

    return {"message": "Ticket passed to another CS successfully"}

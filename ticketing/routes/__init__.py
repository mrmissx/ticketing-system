from .user import router as user_router
from .product import router as product_router
from .ticket import router as ticket_router

__all__ = ["user_router", "product_router", "ticket_router"]

from os import getenv
from sys import argv

from uvicorn import run
from fastapi import FastAPI

from ticketing.database import engine, BASE, get_session
from ticketing.routes import user_router, product_router, ticket_router
from ticketing.utils.seeder import run_seeder

app = FastAPI()
app.include_router(user_router)
app.include_router(ticket_router)
app.include_router(product_router)


def start():
    if "--fresh" in argv:
        session = get_session()
        session.execute("DROP TABLE IF EXISTS ticket")
        session.execute("DROP TABLE IF EXISTS user")
        session.execute("DROP TABLE IF EXISTS product")
        BASE.metadata.create_all(engine)
        run_seeder(session)
        session.commit()

    run(
        app="ticketing.main:app",
        host="0.0.0.0",
        port=int(getenv("PORT", 8000)),
        reload="--reload" in argv or "-r" in argv,
    )

import os

from sqlalchemy import create_engine
from sqlalchemy.orm import declarative_base, sessionmaker, Session

host = os.getenv("DB_HOST")
username = os.getenv("DB_USERNAME")
password = os.getenv("DB_PASSWORD")
database = os.getenv("DB_DATABASE")
BASE = declarative_base()

engine = create_engine(
    f"mysql+pymysql://{username}:{password}@{host}/{database}",
    echo=True,
    connect_args={
        "ssl": {
            "ssl_ca": "/etc/ssl/certs/ca-certificates.crt",
        }
    },
)

_SESSION = None


def get_session() -> Session:
    global _SESSION

    if _SESSION is None:
        Session = sessionmaker(bind=engine)
        _SESSION = Session()
    return _SESSION

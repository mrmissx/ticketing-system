from uuid import uuid4

from sqlalchemy import Column, String, Enum

from .. import BASE


class Ticket(BASE):
    __tablename__ = "ticket"
    id = Column(String(36), primary_key=True)
    title = Column(String(225), nullable=False)
    problem = Column(String(225), nullable=False)
    product_name = Column(String(225), nullable=False)
    product_version = Column(String(225), nullable=False)
    cs_id = Column(String(36), nullable=False)
    creator_id = Column(String(36), nullable=False)
    status = Column(Enum("open", "close"), nullable=False, default="open")

    def __init__(
        self,
        title: str,
        problem: str,
        product_name: str,
        product_version: str,
        cs_id: str,
        creator_id: str,
    ):
        self.id = str(uuid4())
        self.title = title
        self.problem = problem
        self.product_name = product_name
        self.product_version = product_version
        self.cs_id = cs_id
        self.creator_id = creator_id

    def __repr__(self):
        return f"Ticket(id={self.id}, title={self.title}, problem={self.problem}, cs={self.cs_id}, creator={self.creator_id})"

from datetime import datetime
from uuid import uuid4

from sqlalchemy import Column, String, UniqueConstraint

from .. import BASE


class Product(BASE):
    __tablename__ = "product"
    id = Column(String(36), primary_key=True)
    name = Column(String(225), nullable=False)
    version = Column(String(225), nullable=False)
    created_at = Column(String(225), nullable=False)
    deleted_at = Column(String(225), nullable=True)

    __table_args__ = (UniqueConstraint("name", "version"),)

    def __init__(self, name: str, version: str):
        self.id = str(uuid4())
        self.name = name
        self.version = version
        self.created_at = datetime.utcnow()

    def __repr__(self):
        return f"Product(id={self.id}, name={self.name}, version={self.version})"

    def delete(self):
        self.deleted_at = datetime.utcnow()

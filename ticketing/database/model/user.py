from uuid import uuid4
from datetime import datetime

from sqlalchemy import Column, String, Enum, DATETIME

from .. import BASE


class User(BASE):
    __tablename__ = "user"
    id = Column(String(36), primary_key=True)
    username = Column(String(225), nullable=False, unique=True)
    password = Column(String(225), nullable=False)
    role = Column(Enum("user", "cs"), nullable=False, default="user")
    created_at = Column(DATETIME, nullable=False)
    deleted_at = Column(DATETIME, nullable=True)

    def __init__(self, username: str, password: str, role: str = "user"):
        self.id = str(uuid4())
        self.username = username
        self.password = password
        self.role = role
        self.created_at = datetime.utcnow()

    def __repr__(self):
        return f"User(id={self.id}, username={self.username})"

from datetime import datetime, timedelta
from os import getenv
from typing import Any, Dict

from fastapi import HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer
from jwt import encode, decode, InvalidTokenError

SECRET_KEY = getenv("SECRET", None)
if not SECRET_KEY:
    SECRET_KEY = "[SECRET]"
    print("WARNING: No secret key provided. Using default.")


def generate_token(payload: Dict[str, Any]) -> str:
    return encode(payload, SECRET_KEY, algorithm="HS256")


def verify_token(token: str = Depends(OAuth2PasswordBearer("token"))) -> Dict[str, Any]:
    try:
        return decode(token, SECRET_KEY, algorithms=["HS256"])
    except InvalidTokenError:
        raise HTTPException(status_code=400, detail="Invalid token")

from abc import ABC, abstractmethod
from typing import MutableMapping, List
from sqlalchemy.orm import Session

from ticketing.database.model import User, Product


class BaseSeeder(ABC):
    def __init__(self, session: Session) -> None:
        self.session = session

    @abstractmethod
    def run(cls, session: Session) -> None:
        ...


class UserSeeder(BaseSeeder):
    _role = "user"
    _data: List[MutableMapping[str, str]] = [
        {"username": "user1", "password": "user1"},
        {"username": "user2", "password": "user2"},
    ]

    def run(self) -> None:
        for data in self._data:
            user = User(**data, role=self._role)
            self.session.add(user)
        self.session.commit()


class CsSeeder(BaseSeeder):
    _role = "cs"
    _data: List[MutableMapping[str, str]] = [
        {"username": "cs1", "password": "cs1"},
        {"username": "cs2", "password": "cs2"},
    ]

    def run(self) -> None:
        for data in self._data:
            user = User(**data, role=self._role)
            self.session.add(user)
        self.session.commit()


class ProductSeeder(BaseSeeder):
    _data: List[MutableMapping[str, str]] = [
        {"name": "Dota 2", "version": "1.2.21"},
        {"name": "Dota 2", "version": "1.2.24"},
        {"name": "Dota 2", "version": "1.5.0"},
        {"name": "Dota 2", "version": "2.0.0"},
        {"name": "Apex Legends", "version": "5.2.0"},
        {"name": "Apex Legends", "version": "5.2.1a"},
        {"name": "Apex Legends", "version": "5.2.4"},
        {"name": "Apex Legends", "version": "5.3.0"},
    ]

    def run(self) -> None:
        for data in self._data:
            product = Product(**data)
            self.session.add(product)
        self.session.commit()


def run_seeder(session: Session) -> None:
    UserSeeder(session).run()
    CsSeeder(session).run()
    ProductSeeder(session).run()
